package com.exampleCar.Car.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties("car")
@Setter
@Getter
public class Config {
    private List<String> list;
    private List<String> colors;

}


